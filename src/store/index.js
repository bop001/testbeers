import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    beerList: [],
    elementId: 0,
    currentPage: 1,
    isBeerListPending: false,
  },
  mutations: {
    INCREMENT_PAGINATION_PAGE: (state) => {
      state.currentPage += 1;
    },
    SET_BEERS_TO_STORE: (state, beerList) => {
      state.beerList = beerList;
    },
    DELETE_ELEMENT_FROM_LIST: (state, value) => {
      const newList = state.beerList.filter((item) => item.id !== value);
      state.beerList = newList;
    },
    CHANGE_INFO_ELEMENTS: (state, value) => {
      const targetItemIndex = state.beerList.findIndex((item) => item.id === value.id);
      state.beerList[targetItemIndex] = {
        ...state.beerList[targetItemIndex],
        ...value,
      };
    },
    CHANGE_ELEMENT_ID: (state, value) => {
      const { id } = state.beerList.find((item) => item.id === value);
      state.elementId = id;
    },
  },
  actions: {
    GET_BEER_FROM_API({ state }) {
      state.isBeerListPending = true;
      return axios('https://api.punkapi.com/v2/beers', {
        method: 'GET',
        params: {
          page: state.currentPage,
          limit: 25,
        },
      })
        .then((response) => response.data)
        .finally(() => {
          state.isBeerListPending = false;
        });
    },
  },
  getters: {
    BEER_LIST(state) {
      return state.beerList;
    },
    ELEMENT_ID(state) {
      return state.elementId;
    },
  },
  modules: {
  },
});
